
// Grab initial pokemon list to display on page
$('document').ready(function(){

	var count = 0;
	$.get( "api.php?resource=pokemon&offset=0", function( data ) {
		$('main').append('<ul id="pokemon">');

		$.each(data.results, function(i, item) {
			console.log(i + item.name);

			$('#pokemon').append('<li id=' + item.name + " onclick=getDetails(" + "'" + item.name + "'" + ")" + '>' + item.name + '</li>');
			var content = "<div id=" + item.name + "-modal " +  "class=\"modal fade\" role=\"dialog\"> \
				<div class=\"modal-dialog\"><div class=\"modal-content\">\
				<div class=\"modal-header\">\
				<h4 class=\"modal-title\">" + item.name + "</h4>\
				<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\
				</div>\
				<div class=\"modal-body\" id=" + item.name + "-modal-body></div>\
				<div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button></div></div></div></div>";
				$('#pokemon').append(content);		
		});
	}, "json" );
		$('main').append('</ul>');

});


// API result list offset
var offset = 0;

// Load more pokemon data when end of page reached
$(window).scroll(function() {
    if($(window).scrollTop() == $(document).height() - $(window).height()) {
    	console.log("scroll-reached");
    	offset += 50;
           $.get( "api.php?resource=pokemon&offset=" + offset, function( data ) {
		$.each(data.results, function(i, item) {
			console.log(i + item.name);

			$('#pokemon').append('<li id=' + item.name + " onclick=getDetails(" + "'" + item.name + "'" + ")" + '>' + item.name + '</li>');
			var content = "<div id=" + item.name + "-modal " +  "class=\"modal fade\" role=\"dialog\"> \
				<div class=\"modal-dialog\"><div class=\"modal-content\">\
				<div class=\"modal-header\">\
				<h4 class=\"modal-title\">" + item.name + "</h4>\
				<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\
				</div>\
				<div class=\"modal-body\" id=" + item.name + "-modal-body></div>\
				<div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button></div></div></div></div>";
				$('#pokemon').append(content);		
		});
	}, "json" );
    }
});


// Called when specific pokemon element is clicked in link
function getDetails(key) {
	console.log("api.php?resource=pokemon&key=" + key);

	// Download pokemon data
	$.get( ("api.php?resource=pokemon&key=" + key), function( data ) {
		console.log(data.name);

		// Generate modal contents
		var modalContents = ("<ul class=\"pokemondetails\">" + "<li> Height: " + data.height + "</li>" + "<li> Weight: " + data.weight + "</li>" + "<li> Species: " + data.species.name + "</li>" + "<li> Abilities: <ol>");
		$.each(data.abilities, function(i, item) {
			modalContents += "<li>" + item.ability.name + "</li>";
		});
		modalContents += "</ol></ul>";

		modalContents += "<img src=\"images/" + data.id + ".png\"/>"

		// Add modal contents to page and show
		$('#' + key + "-modal-body").html(modalContents);
		}, "json" );

	$('#' + key + "-modal").modal('show');
}


// Called upon search button click
function searchPokemon() {

	// Get search query
	var queryTerm = $("#searchBox").val();
	console.log("search clickd");

	//Generate empty modal element
	var content = "<div id=search-modal " +  "class=\"modal fade\" role=\"dialog\"> \
				<div class=\"modal-dialog\"><div class=\"modal-content\">\
				<div class=\"modal-header\">\
				<h4 class=\"modal-title\">" + queryTerm + "</h4>\
				<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\
				</div>\
				<div class=\"modal-body\" id=\"search-modal-body\"></div>\
				<div class=\"modal-footer\"><button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button></div></div></div></div>";	

	// Add to page, but don't show
	$('#search-modal-container').html(content);

	console.log("api.php?resource=pokemon&key=" + queryTerm);

	// Get pokemon data using search query
	$.get( ("api.php?resource=pokemon&key=" + queryTerm), function( data ) {
		console.log(data.name);

		// Add contents to modal and show
		var modalContents = ("<ul>" + "<li> Height: " + data.height + "</li>" + "<li> Weight: " + data.weight + "</li>" + "<li> Species: " + data.species.name + "</li>" + "<li> Abilities: <ol>");
		$.each(data.abilities, function(i, item) {
			modalContents += "<li>" + item.ability.name + "</li>";
		});
		modalContents += "</ol></ul>";

		modalContents += "<img src=\"images/" + data.id + ".png\"/>"

		console.log("contents formed");
		$("#search-modal-body").html(modalContents);
		}, "json" );

	$("#search-modal").modal('show');
}
