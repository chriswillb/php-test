<?php
require_once "vendor/autoload.php";
use PokePHP\PokeApi;

define("LIST_LIMIT", 50);
$api = new PokeApi;

// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];

$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// Generate cachefile name from md5 of url
$cacheFile = '.\cache' . DIRECTORY_SEPARATOR . md5($url);

// If that cachefile already exists echo it if cached in last hour and return
if (file_exists($cacheFile)) {
  $fh = fopen($cacheFile, 'r');
  $cacheTime = trim(fgets($fh));

      // if data was cached recently, echo and return
  if ($cacheTime > strtotime('-60 minutes')) {
    echo fread($fh, filesize($cacheFile));
    return;
  }

      // else delete cache file
  fclose($fh);
  unlink($cacheFile);
}

// Retrieve query string params (api resource and pokemon id/name)
$parts = parse_url($url);
parse_str($parts['query'], $query);

$resource = $query['resource'];


// If we have a key then we pass that to the requested endpoint
if (isset($query['key'])) {
  $key = $query['key'];
  // Check requested resource and execute request with given key
  switch ($resource) {
    case 'pokemon':  
      $result = $api->pokemon($key);
      break;
    case 'pokemon-species':
      $result = $api->pokemonSpecies($key);
      break;
  }
} 

// Else check if offset is given for resource list and retrieve accordingly
elseif (isset($query['offset'])) {
  $offset = $query['offset'];
  switch ($resource) {
    case 'pokemon':  
      $result = $api->resourceList('pokemon', LIST_LIMIT, $offset);
      break;
    case 'pokemon-species':
      $result = $api->resourceList('pokemon-species', LIST_LIMIT, $offset);
      break;
  }
}

// Else pass nothing and retrieve default resource list
else{
  switch ($resource) {
    case 'pokemon':  
      $result = $api->pokemon('');
      break;
    case 'pokemon-species':
      $result = $api->pokemonSpecies('');
      break;
  }
}

// Cache and echo results
if (!empty($result)) {
  $fh = fopen($cacheFile, 'w');
  fwrite($fh, time() . "\n");
  fwrite($fh, $result);
  fclose($fh);
  echo $result;
} 

// Return 404 if no result
else {
  http_response_code(404);
}
?>


